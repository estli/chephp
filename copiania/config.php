<?php

/**
 * Configuration for database connection
 *
 */

$host       = "localhost:3306";
$username   = "user";
$password   = "root";
$dbname     = "database";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );