# ChePHP (Che IDE & PHP con MySQL en Open Shift)

## Este repo es para desarrollar en OpenShift con el IDE Eclipse Che

Se usa PHP con la base de datos MySQL

**Se necesita exportar un .sql porque se borran los archivos al cerrar el workspace**

Archivo SQL
bd.sql | Archivo .sql para importar

Tutorial para crear tablas:
https://www.tutorialspoint.com/mysql/mysql-create-tables.htm

Tutorial para exportar base de datos:
https://www.a2hosting.com.mx/kb/developer-corner/mysql/import-and-export-a-mysql-database

Tutorial para el SELECT:
https://www.tutorialrepublic.com/php-tutorial/php-mysql-select-query.php

**Repo creado por Marco Antonio Castillo Reyes**